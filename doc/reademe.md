# 查询余额模块  


代码示例
-------  
~~~python
def seeresult(name):# 保证每次设置完成后都要传name参数回主界面，否则主界面不认人
    """查询余额,直接返回该账户的余额，没啥好说的，直接返回数值"""
    with open(path+'/account.json','r',encoding='utf-8') as account:#从数据库读取文件
        account1=account.read()#读取成可读形式
        acc=json.loads(account1)#因为数据库数据是json形式，`需转换成python看得懂的形式`
        for i in range(len(acc['users'])):#循环查找传入的用户名
            """这块要循环，然后判断账号正确性"""
            if name==acc['users'][i]['name']:#如果找到该用户就输出他的余额
                return acc['users'][i]['result']
            elif i==len(acc['users'])-1: #循环到最后一个用户如果还是没找到返回False(错误)
                return False
~~~
### 代码逻辑  
传入用户名，读取用户列表（也就是account.json），然后循环到，传出该用户的余额  
### 细讲
先使用with函数把数据库（account.json，下文中不再出现）中的json数据读取出来，在当中使用read()函数读取成字符串，在把json读取成python能看懂的字符串，再然后使用for函数循环数据库中的user（用户表），以用户数条件循环。再次使用if函数选择获取到的用户名，输出他的钱就可以了。再最后，如果循环到最后一个人的时候，证明for函数循环到了最后，说明这个人不存在数据库中，输出错误

# 更改账户密码模块
代码示例
-----
~~~python
def setpassword(name,repass):# 保证每次设置完成后都要传name参数回主界面，否则主界面不认人
    """设置密码,传入设置的账号，传回布尔值，没啥可说的，找到了就删除，找不到就错误了"""
    with open(path+'/account.json','r',encoding='utf-8') as account:
        account1=account.read()#读取文件内容
        acc=json.loads(account1)# 读取成可读字符串
        for i in range(len(acc['users'])):#循环寻找自己的用户位置
            """这块要循环，找相同的用户名字，找到修改即可"""
            if name==acc['users'][i]['name']: #寻找和当前一样的用户，找到然后修改
                acc['users'][i]['password']=repass #修改用户名核心
                acc=json.dumps(acc)
                with open(path+'/account.json','w',encoding='utf-8') as saveAccount:
                    saveAccount.write(str(acc))
                return True
            elif i==len(acc['users'])-1:
                return False #程序绝对错误了，这个直接报错，没得商量
~~~
### 代码逻辑
传入用户的名字和要设置的密码  
- 先从数据库中寻找用户的名字
- 循环找到用户的名字
- 找到后修改用户密码
- 然后写入数据库  
### 细讲  
设置密码函数只需要传入name和repass变量就可以了，和上一个人说的一样，都是使用的with函数读取数据库（account.json）里面的用户表，从json数据读取出来，在当中使用read()函数读取成字符串，在把json读取成python能看懂的字符串，使用for循环寻找这个人的名字，找到后，修改json中的password字符，赋值进去，再写入数据库就可以了，如果到了最后一个用户，那就是没有找到这个用户的信息，返回错误
# 删除账户模块
  
代码示例
----
~~~python
def delaccount(name,password):# 保证每次设置完成后都要传name参数回主界面，否则主界面不认人
    """删除账号，传回name，password，验证是否为账号主人，验证成功，删除账号，返回True，验证不是该主人，返回错误，False"""
    with open(path+'/account.json','r',encoding='utf-8') as files:
        files=files.read()
        acc=json.loads(files)
    for i in range(len(acc['users'])):
        """这块要循环，然后判断账号正确性"""
        if name==acc['users'][i]['name'] and password==acc['users'][i]['password']:
            del acc['users'][i]
            acc=json.dumps(acc)#写成json之后传回去
            with open(path+'./account.json','w',encoding='utf-8') as writ:
                writ.write(str(acc))
            return True
        elif i==len(acc['users'])-1:
            return False # 你账号输入错误了
~~~
### 代码逻辑
传入用户的名字和密码  
- 先从数据库中寻找用户的名字
- 循环找到用户的名字和密码
- 找到后删除该用户列
- 然后写入数据库  
### 细讲
。。。。。。。。。




---------------------------------------

# 取钱模块
代码实例
----
~~~python
def outresult(name,namber):# 保证每次设置完成后都要传name参数回主界面，否则主界面不认人
    """取钱，同上,但是要考虑负数的情况，负数返回0"""
    namber=int(namber)#将传入的namber转换为整数，反正后面报错
    with open(path+'/account.json','r',encoding='utf-8') as account:#读取数据库文件
        account1=account.read()#读取成可读
        acc=json.loads(account1)#读取json文件，转换为python可以看得懂的形式
    for i in range(len(acc['users'])):
        """这块要循环，然后判断账号正确性"""
        if name==acc['users'][i]['name']:
            namber = acc['users'][i]['result'] - namber# 这块相减,然后扣钱，扣到0了，就返回0
            if namber < 0:
                return 0
            else:
                with open(path+'/account.json','w',encoding='utf-8')as files:#读取数据库文件，
                    acc['users'][i]['result']=namber# 将读取到的数据库文件赋值入刚刚相减后的变量
                    acc=json.dumps(acc)# 将python编写成json
                    files.write(str(acc))#写入数据库
                return True
        elif i==len(acc['users'])-1: #循环到最后一个用户时，如果没有，则传回false
            return False
~~~
### 代码逻辑
传入用户名和要取的钱
- 先从数据库中寻找用户的名字
- 循环找到用户的名字和密码
- 找到后扣除相应的钱，如果扣到负数直接函数返回0
- 如果没有负数就将扣除之后的钱写到数据库
- 如果循环到最后，就是没有找到这个人在数据库中的信息，返回错误

### 细讲
先传入name，namber这个两个变量，然后保证namber这个变量为数值型，再从数据库中读取json函数，然后使用json这个内置函数包转换成python看得懂的列表。再for循环找这个用户的用户名，找到后扣除相应的钱数，如果到负数后，直接返回0，说明这个账户不够。如果没有到负数，再把扣除相应的钱后的数值赋值到之后内存数据库中，再写入数据库

# 存钱模块
代码示例
----
~~~python
def severesult(name,namber):# 保证每次设置完成后都要传name参数回主界面，否则主界面不认人
    """存钱，取整还是放在主界面吧，这块就做加减算了，传过来name、namber，名字，和要取的钱，然后返回布尔值"""
    namber=int(namber)#将传入的namber转换为整数，反正后面报错
    with open(path+'/account.json','r',encoding='utf-8') as account:#读取数据库文件
        account1=account.read()#读取成可读字符串
        acc=json.loads(account1)#将json转换为
        for i in range(len(acc['users'])):# 循环找传过来的账户名
            """这块要循环，然后判断账号正确性"""
            if name==acc['users'][i]['name']:# 如果找到了
                namber += acc['users'][i]['result']# 相加传过来的函数和自己数据库中的变量
                with open(path+'/account.json','w')as files:# 写入数据库
                    acc['users'][i]['result']=namber # 将数据库中的数值赋值为相加之后的数值
                    acc=json.dumps(acc)#转换为json
                    files.write(str(acc))#写入数据库
                return True  
            elif i==len(acc['users'])-1: #未查询到账户，返回错误
                return False
~~~
### 代码逻辑
传入用户名和要取的钱
- 先从数据库中寻找用户的名字
- 循环找到用户的名字和密码
- 找到后相加钱
- 到最后，没有找到这个人在数据库中的信息，返回错误
### 细讲
先传入name，namber这个两个变量，然后保证namber这个变量为数值型，再从数据库中读取json函数，然后使用json这个内置函数包转换成python看得懂的列表。再for循环找这个用户的用户名，找到后相加相应的钱数，至后的数值赋值到之后的内存数据库中，再写入数据库