import json
import os
"""文件处理单元"""

path1 = os.path.abspath(__file__)
path = os.path.split(path1)[0]#获取工作目录(通用型)
if not os.path.exists(path+'/account.json'):#没有account就创建一个
    print('没有account.json文件，将自动创建，默认账号密码为\nzhangs\n123456')
    files=open(path+'/account.json','w',encoding='utf-8')
    files.write(r'{"users":[{"name":"zhangs","password":"jfdkslf","result":100},{"name":"lisi","password":"jfdkslffd","result":100}]}')# 你**居然没有文件夹，我给你**创建一个
    files.close()

def fes(name,password):
    """识别主界面传入的账号和密码，传回布尔值"""
    print(name,password)
    with open(path+'/account.json','r',encoding='utf-8') as account:#读取
        account1=account.read()
        acc=json.loads(account1)
        print(len(acc['users']))
        for i in range(len(acc['users'])):
            """这块要循环，然后判断账号正确性"""
            if name==acc['users'][i]['name'] and password==acc['users'][i]['password']:
                return True
            elif i==len(acc['users'])-1:
                return False

def regf(name,password):
    """传入注册账户，编写成json，传入account.json
    如果有相同名称，返回False
    
    """
    with open(path+'/account.json','r',encoding='utf-8') as account:#读取
        account1=account.read()
        acc=json.loads(account1)
        for i in range(len(acc['users'])):
            print('用户为',acc['users'][i])
            print('用户人数',len(acc['users']))
            print(i)
            """这块要循环，然后判断账号是否相同"""
            if name==acc['users'][i]['name'] :
                print('您输入的账号相同')
                return False
            elif i==len(acc['users'])-1:
                with open(path+'/account.json', 'r') as f:
                    dict = json.load(f)
                    dict2={'name':name,'password':password,'result':100}
                    dict['users'].append(dict2)
                    dictj=json.dumps(dict)

                with open(path+'/account.json','w') as t:
                    t.write(str(dictj))
                    return True

def setaccount(name,rname): # 保证每次设置完成后都要传name参数回主界面，否则主界面不认人
    """设置账户名,传入的name为当前用户名，rname为修改的用户名，写入成功传回True，如果没有找到用户返回False。如果修改的用户名与他人一样，传回数字型0"""
    with open(path+'/account.json','r',encoding='utf-8') as account:
        account1=account.read()#读取文件内容
        acc=json.loads(account1)# 读取成可读字符串
        pass
        for i in range(len(acc['users'])):
            """这块要循环，然后判断账号是否相同,并验证传回的rename不能和他人一样，一样就传回0"""
            #这块必须找相同的用户名，找到返回0，没找到
            if rname==acc['users'][i]['name']:
                continue
            else:
                if name==acc['users'][i]['name']: #寻找和当前一样的用户，找到然后修改
                    acc['users'][i]['name']=rname #修改用户名核心
                    acc=json.dumps(acc)
                    with open(path+'/account.json','w',encoding='utf-8') as saveAccount:
                        saveAccount.write(str(acc))
                    return True
                elif i==len(acc['users'])-1:
                    return False #程序绝对错误了，这个直接报错，没得商量
        return 0
    
def setpassword(name,repass):# 保证每次设置完成后都要传name参数回主界面，否则主界面不认人
    """设置密码,传入设置的账号，传回布尔值，没啥可说的，找到了就删除，找不到就错误了"""
    with open(path+'/account.json','r',encoding='utf-8') as account:
        account1=account.read()#读取文件内容
        acc=json.loads(account1)# 读取成可读字符串
        for i in range(len(acc['users'])):
            """这块要循环，找相同的用户名字，找到修改即可"""
            if name==acc['users'][i]['name']: #寻找和当前一样的用户，找到然后修改
                acc['users'][i]['password']=repass #修改用户名核心
                acc=json.dumps(acc)
                with open(path+'/account.json','w',encoding='utf-8') as saveAccount:
                    saveAccount.write(str(acc))
                return True
            elif i==len(acc['users'])-1:
                return False #程序绝对错误了，这个直接报错，没得商量

def delaccount(name,password):# 保证每次设置完成后都要传name参数回主界面，否则主界面不认人
    """删除账号，传回name，password，验证是否为账号主人，验证成功，删除账号，返回True，验证不是该主人，返回错误，False"""
    with open(path+'/account.json','r',encoding='utf-8') as files:
        files=files.read()
        acc=json.loads(files)
    for i in range(len(acc['users'])):
        """这块要循环，然后判断账号正确性"""
        if name==acc['users'][i]['name'] and password==acc['users'][i]['password']:
            del acc['users'][i]
            acc=json.dumps(acc)#写成json之后传回去
            with open(path+'./account.json','w',encoding='utf-8') as writ:
                writ.write(str(acc))
            return True
        elif i==len(acc['users'])-1:
            return False # 你账号输入错误了
    
def seeresult(name):# 保证每次设置完成后都要传name参数回主界面，否则主界面不认人
    """查询余额,直接返回该账户的余额，没啥好说的，直接返回数值"""
    with open(path+'/account.json','r',encoding='utf-8') as account:#从数据库读取文件
        account1=account.read()#读取成可读形式
        acc=json.loads(account1)#因为数据库数据是json形式，需转换成python看得懂的形式
        for i in range(len(acc['users'])):#循环查找传入的用户名
            """这块要循环，然后判断账号正确性"""
            if name==acc['users'][i]['name']:#如果找到该用户就输出他的余额
                return acc['users'][i]['result']
            elif i==len(acc['users'])-1: #循环到最后一个用户如果还是没找到返回False(错误)
                return False

def severesult(name,namber):# 保证每次设置完成后都要传name参数回主界面，否则主界面不认人
    """存钱，取整还是放在主界面吧，这块就做加减算了，传过来name、namber，名字，和要取的钱，然后返回布尔值"""
    namber=int(namber)
    with open(path+'/account.json','r',encoding='utf-8') as account:#读取
        account1=account.read()
        acc=json.loads(account1)
        for i in range(len(acc['users'])):
            """这块要循环，然后判断账号正确性"""
            if name==acc['users'][i]['name']:
                namber += acc['users'][i]['result']# 这块相加
                with open(path+'/account.json','w')as files:
                    acc['users'][i]['result']=namber
                    acc=json.dumps(acc)
                    files.write(str(acc))
                return True
            elif i==len(acc['users'])-1: #未查询到账户余额
                return False

def outresult(name,namber):# 保证每次设置完成后都要传name参数回主界面，否则主界面不认人
    """取钱，同上,但是要考虑负数的情况，负数返回0"""
    namber=int(namber)
    with open(path+'/account.json','r',encoding='utf-8') as account:#读取
        account1=account.read()
        acc=json.loads(account1)
    for i in range(len(acc['users'])):
        """这块要循环，然后判断账号正确性"""
        if name==acc['users'][i]['name']:
            namber = acc['users'][i]['result'] - namber# 这块相减,然后扣钱，扣到0了，滚蛋，资本家不会找你了
            if namber < 0:
                return 0
            else:
                with open(path+'/account.json','w',encoding='utf-8')as files:
                    acc['users'][i]['result']=namber
                    acc=json.dumps(acc)
                    files.write(str(acc))
                return True
        elif i==len(acc['users'])-1: #未查询到账户余额
            return False
def transfer(name,inName,namber):# 保证每次设置完成后都要传name参数回主界面，否则主界面不认人
    """转账,传过来name,要转的账户还有要转的钱，返回布尔值（错误就是没有找到账户）,如果当前用户转出去的钱太少，返回0"""
    namber=int(namber)
    with open(path+'/account.json','r',encoding='utf-8') as account:#读取
        account1=account.read()
        acc=json.loads(account1)#acc为解析出来的python可读字符串
    for i in range(len(acc['users'])):
        """循环找到要转的账户，然后扣除相应金额，然后再循环，找到被转用户，加入相应金额，写入数据库"""
        if name==acc['users'][i]['name']:
            losernamber = acc['users'][i]['result'] - namber# 这块相减,然后扣钱，扣到0了，滚蛋，资本家不会找你了
            if losernamber < 0:
                return 0
            else:
                with open(path+'/account.json','w',encoding='utf-8') as account:#读取
                    acc['users'][i]['result']=losernamber
                    acc2=json.dumps(acc)#保证acc不要自己赋值，不然后面的acc无法获取长度
                    account.write(str(acc2))
                    break# 整完退出去
        elif i==len(acc['users']):#未找到账户
            return False
    for n in range(len(acc['users'])):
        if inName == acc['users'][n]['name']:
            getnamber = acc['users'][n]['result'] + namber
            with open(path+'/account.json','w',encoding='utf-8') as files:
                acc['users'][n]['result']=getnamber
                acc3=json.dumps(acc)
                files.write(str(acc3))
            return True
        elif i==len(acc['users'])-1: #未查询到账户余额
            return False
##########调试区域##################