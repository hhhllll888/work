from base64 import encode
import PySimpleGUI as sg
import pcfile
"""该项目为学校实践作业，使用Pysimple的一个atm系统。开放代码以供学习。
https://gitee.com/hhhllll888
还得改改弹窗逻辑
"""
            
def inin():
    """入口gui界面"""
    
    layoute = [
            [sg.Text('请输入您的账号密码',)],
            [sg.Text('账号', size=(15, 1)), sg.InputText('1',key='-name-')],
            [sg.Text('密码', size=(15, 1)), sg.Input('1',password_char='*',key='-pwd-')]
        ]
    layout=[
        [sg.Frame('请登陆',layout=layoute,title_location='n'),],
        [sg.Button('确认') ,sg.B('注册账户'),sg.B('退出')]
    ]
    window=sg.Window('ATM管理系统',layout=layout,font='Arial',auto_size_text=True)
    
    while True:
        event,values = window.read()
        name,password = values['-name-'],values['-pwd-']
        print(name,password)
        print('values是',values,'event是',event)
        if event=="注册账户":
            window.close()
            regui()
            break
        elif event==None or event=='退出':#退出
            window.close()
            break
        elif name==''or password=='':#有一个为空都不行
                sg.popup_error('账号为空')
                window.close()
                inin()
                break
        elif event=='确认':
            okr=pcfile.fes(name,password)
            if okr:#判断账号正确性
                window.close()#账号验证正确
                mastegui(name)
                break
            else:
                msg='密码输入错误'
                sg.popup_ok(msg)
                window.close()
                inin()
                break

            

def regui():
    """注册框"""
    layout = [
            [sg.T('账户注册')],
            [sg.T('请输入账户名',size=(11,1)),sg.InputText('',key="name")],
            [sg.T('请输入密码',size=(11,1)),sg.InputText('',key="password")],
            [sg.B('确认')]
        ]
    window = sg.Window('账户注册', layout=layout,font=('Arial'),auto_size_text=True)
    
    while True :
        event1, values1 = window.read()
        rname,rpassword=values1['name'],values1['password']
        if event1==None:
            window.close()
            inin()
            break
        elif event1=='确认':
            if rname=='' or rpassword=='':
                sg.popup_error('请输入账户密码')
                window.close()
                regui()
                break
            elif pcfile.regf(rname,rpassword):
                sg.popup_ok('您的账号注册成功')
                window.close()
                inin()
                break
            else:
                sg.popup_error('您的账号名称与以存在数据库中，注册失败')
                window.close()
                regui()
                break
            
def mastegui(name):
    """主页面"""
    
    Frame1=[
        [sg.B('设置账户名')],[sg.B('修改密码')],[sg.B('删除账户')]
    ]
    Franme2=[
        [sg.B('查询余额')],[sg.B('存钱')],[sg.B('取钱')],[sg.B('转账')]
    ]
    layout=[
        [sg.Frame('账号管理',layout=Frame1,title_location='n'),sg.Frame('余额管理',layout=Franme2,title_location='n')],[sg.B('退出')]
    ]
    window=sg.Window('您好'+name+'欢迎使用atm系统',layout=layout,auto_size_text=True,font='Arial')
    while True:
        event,values=window.read()
        if event==None:
            break
        if event=='设置账户名':
            window.close()
            setaccountgui(name)
            break
        if event=='修改密码':
            window.close()
            setpasswordgui(name)
            break
        if event=='删除账户':
            window.close()
            delaccountgui(name)
            break
        if event=='查询余额':
            window.close()
            seeresultgui(name)
            break
        if event=='存钱':
            window.close()
            severesultgui(name)
            break
        if event=='取钱':
            window.close()
            outresultgui(name)
            break
        if event=='转账':
            window.close()
            transfergui(name)
            break
        window.close()
def setaccountgui(name):
    """设置账户名的界面"""
    layout = [
    [sg.T('账户设置')],[sg.T('您要设置的账户名'),sg.In('',key="-rename-")],[sg.B('确认'),sg.B('退出')]
    ]
    window = sg.Window("请设置您的账户名", layout=layout,font='宋体')
    while True:
        event, values = window.read()
        print('输入的数据',name,values['-rename-'])
        if event==None or event=='退出': # 退出事件
            window.close()
            mastegui(name)
            break
        elif event=='确认':
            if values['-rename-']=='':
                sg.popup_error('您未设置账户名')
                window.close()
                setaccountgui(name)
                break
            elif name==values['-rename-']:
                    sg.popup_ok('好歹设置一个其他名字吧。')
                    window.close()
                    setaccountgui(name)
                    break
            else:
                okr=pcfile.setaccount(name,values['-rename-'])
                if okr == 0:
                    sg.popup_ok('您输入的名称与他人一样，请重新想一个用户名')
                    window.close()
                    setaccountgui(name)
                    break
                else:
                    if okr: # True和False分析
                        sg.popup_ok('您的账户名已成功修改,请重新登陆')
                        window.close()
                        inin()
                        break
                    else:
                        sg.popup_ok('怎么回事呢(＠_＠;)，错误代码-1，无法在数据库中找到您的信息，请反馈作者')
                        window.close()
                        mastegui(name) 
                        break          
def setpasswordgui(name):
    """设置密码的界面"""
    layout = [
    [sg.T('账户设置')],[sg.T('您要设置的密码'),sg.In('',key="-repass-")],[sg.B('确认'),sg.B('退出')]
    ]
    window = sg.Window("请设置您的密码", layout=layout,font='宋体')
    while True:
        event, values = window.read()
        if event==None or event=='退出': # 退出事件
            window.close()
            mastegui(name)
            break
        elif event=='确认':
            if values['-repass-']=='':
                sg.popup_error('您未设置密码')
                window.close()
                setpasswordgui(name)
                break
            else:
                okr=pcfile.setpassword(name,values['-repass-'])
                if okr: # True和False分析
                    sg.popup_ok('您的账户密码已成功修改,请重新登陆')
                    window.close()
                    inin()
                    break
                else:
                    sg.popup_ok('怎么回事呢(＠_＠;)，错误代码-1，无法在数据库中找到您的信息，请反馈作者')
                    window.close()
                    mastegui(name) 
                    break

def delaccountgui(name):
    """删除账号页面"""
    layout = [
    [sg.T('请验证您的账号')],[sg.T('账号'),sg.In('',key="-name-")],[sg.T('密码'),sg.In('',key="-password-",password_char='*')],
    [sg.B('确认'),sg.B('退出')]
    ]
    window = sg.Window("删除账号", layout=layout,font='宋体')
    while True:
        event, values = window.read()
        if event==None or event=='退出': # 退出事件
            window.close()
            mastegui(name)
            break
        elif event=='确认':
            if values['-name-']=='' or values['-password-']=='':
                sg.popup_error('您未输入账号密码')
                window.close()
                delaccountgui(name)
                break
            else:
                okr=pcfile.delaccount(values['-name-'],values['-password-'])
                if okr: # True和False分析
                    sg.popup_ok('您的账户成功删除,请重新登陆')
                    window.close()
                    inin()
                    break
                else:
                    sg.popup_ok('怎么回事呢(＠_＠;)，错误代码-1，无法在数据库中找到您的信息，请反馈作者')
                    window.close()
                    mastegui(name) 
                    break
def seeresultgui(name):
    """设置查询余额界面，就弹一个ok框算了，"""
    result=pcfile.seeresult(name)
    if result==False :
        sg.popup_error('啊哦。。出问题了😥',font="宋体")
    sg.popup_ok('您的账户余额为',result)
    mastegui(name)

def severesultgui(name):
    """设置存钱界面，好像只能存整数倍的价钱，所以要判断是否是整钱，最低100，这块做判断"""
    sg.theme('default1')
    layout = [
            [sg.T('请输入存钱的数目')],
            [sg.T('请输入钱'),sg.InputText('',key="money")],
            [sg.B('确认')]
        ]
    window = sg.Window('我要存钱', layout=layout,font=('宋体',15))
    event, values = window.read()
    while True :
        if event==None:
            mastegui(name)
            break
        elif event=="确认":#先测试下传过来的是不是数字，可以转换成数字，再进行下一步
            if values['money'] == '':
                sg.popup_ok("请输入钱")
                window.close()
                severesultgui(name)
                break
            else:
                try:
                    money=int(values['money'])
                except ValueError:
                    sg.popup_ok('你输入别的干嘛')# 加个彩蛋
                    window.close()
                    severesultgui(name)
                    break
                else:
                    if values['money'][-2:] == '00':
                        okr=pcfile.severesult(name,money)
                        if okr==False:
                            sg.popup_ok('程序出错了')
                            window.close()
                            severesultgui(name)
                            break
                        elif okr==True:
                            sg.popup_ok('存入成功')
                            window.close()
                            mastegui(name)
                            break
                    else:
                        sg.popup_ok('请输入整数')
                        window.close()
                        severesultgui(name)
                        break
        window.close()
    window.close()
def outresultgui(name):
    """设置取钱界面，同上"""
    sg.theme('default1')
    layout = [
            [sg.T('请输入取钱的数目')],
            [sg.T('请输入钱'),sg.InputText('',key="money")],
            [sg.B('确认'),sg.B('取消')]
        ]
    window = sg.Window('我要取钱', layout=layout,font=('宋体',15))
    event, values = window.read()
    while True :
        if event==None or event=='取消':
            window.close()
            mastegui(name)
            break
        elif event=="确认":#先测试下传过来的是不是数字，可以转换成数字，再进行下一步
            if values['money'] == '':
                sg.popup_ok("请输入钱")
                window.close()
                outresultgui(name)
                break
            else:
                try:
                    money=int(values['money'])
                except ValueError:
                    sg.popup_ok('你输入别的干嘛')# 加个彩蛋
                    window.close()
                    severesultgui(name)
                    break
                else:
                    if values['money'][-2:] == '00':
                        okr=pcfile.outresult(name,money)
                        if okr==False:
                            sg.popup_ok('程序出错了，未找到您的账户')
                            window.close()
                            severesultgui(name)
                            break
                        elif okr==0:
                            sg.popup_error('您的老板没有给您这么多钱🙃')
                            window.close()
                            severesultgui(name)
                            break
                        elif okr==True:
                            sg.popup_ok('取出成功')
                            window.close()
                            mastegui(name)
                            break
                    else:
                        sg.popup_ok('请输入整数')
                        window.close()
                        severesultgui(name)
                        break
        window.close()
    window.close()

def transfergui(name):
    """设置转账界面"""
    sg.theme('default1')
    layout = [
            [sg.T('请输入要转账的人')],
            [sg.T('请输入他/她的账户名'),sg.InputText('',key="inName")],
            [sg.T('要转的价钱'),sg.In('',key="namber")],
            [sg.B('确认'),sg.B('取消')]
        ]
    window = sg.Window('我要转账', layout=layout,font=('宋体',15))
    event, values = window.read()
    while True :
        if event==None or event=='取消':
            window.close()
            mastegui(name)
            break
        elif event=="确认":#先测试下传过来的是不是数字，可以转换成数字，再进行下一步
            if values['inName'] == '':
                sg.popup_ok("您没有输入要转账的用户")
                window.close()
                transfergui(name)
                break
            elif values['namber'] == '':
                sg.popup_ok("您没有输入要转的价钱")
                window.close()
                transfergui(name)
                break
            else:
                try:
                    money=int(values['namber'])
                except ValueError:
                    sg.popup_ok('你输入别的干嘛')# 加个彩蛋
                    window.close()
                    transfergui(name)
                    break
                else:
                    if values['namber'][-2:] == '00':
                        okr=pcfile.transfer(name,values['inName'],values['namber'])
                        if okr==False:
                            sg.popup_ok('未找到您要转账的账户')
                            window.close()
                            transfergui(name)
                            break
                        elif okr==0:
                            sg.popup_error('您没有这么多钱可以给他/她🙃')
                            window.close()
                            transfergui(name)
                            break
                        elif okr==True:
                            sg.popup_ok('转账成功')
                            window.close()
                            mastegui(name)
                            break
                    else:
                        sg.popup_ok('请输入整数')
                        window.close()
                        transfergui(name)
                        break
        window.close()
    window.close()

if __name__=="__main__":#调试区域
    inin()
